import fancybox from '@fancyapps/fancybox'; // eslint-disable-line no-unused-vars
import headerNavigationSublistSwitcher from '../blocks/header/header-navigation/header-navigation';
import headerMobileSwitcher from '../blocks/header/header-mobile-switcher/header-mobile-switcher';
import tabs from '../blocks/tabs/tabs';
import showHide from '../blocks/show-hide/show-hide';
import productBuy from '../blocks/product-buy/product-buy';
import instaWidget from '../blocks/instagram-widget/instagram-widget';

import Tip from '../blocks/tip/tip';
import counters from './modules/counters';
import video360Init from '../blocks/video-js/video-js';
import Slider from '../blocks/slider/slider';

const countersCodeHead = `
  <script type="text/javascript">
      var __cs = __cs || [];
      __cs.push(["setCsAccount", "qG8oixpQwf1BBKAhgMiT6cRCawAQWXv7"]);
  </script>
  <script type="text/javascript" async defer src="https://app.comagic.ru/static/cs.min.js"></script>
`;
const countersCodeBody = `
  <script type="text/javascript">
      window.dataLayer = window.dataLayer || [];
  </script>
  <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter32237999 = new Ya.Metrika({ id:32237999, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true, ecommerce:"dataLayer" }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script> <!-- /Yandex.Metrika counter -->
`;

counters(countersCodeHead, 'head');
counters(countersCodeBody);

headerNavigationSublistSwitcher();
headerMobileSwitcher();
tabs();
showHide();
productBuy();
instaWidget();

const tips = Array.from(document.querySelectorAll('.tip'));
tips.forEach((item) => {
  const tip = new Tip(item, '.tip__link', '.tip__body', 'tip__body--active');
  tip.init();
});

// eslint-disable-next-line no-unused-vars
const discountsSlider = document.querySelector('.discounts-slider .discounts-slider__slider');
if (discountsSlider) {
  const discountsSliderInstance = new Slider(discountsSlider, {
    navigation: {
      nextEl: '.discounts-slider__navigation--next',
      prevEl: '.discounts-slider__navigation--prev',
      disabledClass: 'discounts-slider__navigation--disabled',
    },
    pagination: {
      el: '.discounts-slider__pagination',
      type: 'bullets',
    },
  });
  discountsSliderInstance.init();
}

const countdowns = document.querySelectorAll('.countdown');
countdowns.forEach((cd) => {
  const date = cd.dataset.date.split(',');
  // eslint-disable-next-line no-undef
  jQuery(cd).countdown({ timestamp: new Date(...date) });
});

// Скрипт для стилей для пункта меню, который является разделом и ссылкой одновременно
const headerNavigationLinks = document.querySelectorAll('.header-navigation__sublist-item a');
headerNavigationLinks.forEach((headerNavigationLink) => {
  if (headerNavigationLink.innerHTML === 'Акция') headerNavigationLink.classList.add('header-navigation__sublist-link');
});

video360Init();
const homeAbout = document.querySelector('.home-about .slider__slides');
if (homeAbout) {
  const homeAboutSliderInstance = new Slider(homeAbout, {
    autoHeight: true,
    on: {
      slideChange: () => {
        const videoSlides = homeAbout.querySelectorAll('.home-about__slide--video');
        videoSlides.forEach((video, index) => {
          const videoItem = video.querySelector('video');
          if (index !== homeAboutSliderInstance.activeIndex) videoItem.pause();
        });
      },
    },
  });
  homeAboutSliderInstance.init();
}
