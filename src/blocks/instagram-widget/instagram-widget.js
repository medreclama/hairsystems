const instaWidget = (settings = { element: '' }) => {
  const selector = settings.element || '.instagram-widget';
  const inwidget = Array.from(document.querySelectorAll(selector));

  class InstaWidget {
    constructor(sets = {
      elem: '', selector: '', userId: '', title: '',
    }) {
      this.elem = sets.elem;

      const url = sets.url || {};

      if (settings.accessToken) {
        this.url = {
          getName: `https://graph.instagram.com/me?fields=username&access_token=${settings.accessToken}`,
          getMedia: `https://graph.instagram.com/me/media?fields=permalink,media_type,media_url,thumbnail_url,caption&access_token=${settings.accessToken}`,
          refreshToken: `https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=${settings.accessToken}`,
        };
      } else {
        this.url = {
          getName: url.getName || '/insta-fetch.php?get_user_name=Y',
          getMedia: url.getMedia || '/insta-fetch.php',
          refreshToken: url.refreshToken || '/insta-fetch.php?refresh=Y',
        };
      }
    }

    async getUserName() {
      const response = await fetch(this.url.getName);
      const user = await response.json();
      return user.username;
    }

    init() {
      new Promise((res) => {
        res(this.getUserName());
      })
        .then((res) => {
          this.userId = settings.userId ? settings.userId : res;
          if (this.elem.dataset.title) this.title = this.elem.dataset.title;
          else this.title = settings.title ? settings.title : res;
        })
        .then(() => {
          if (this.elem.children.length > 0 || !this.url) {
            return;
          }
          const list = document.createElement('div');
          const title = document.createElement('div');
          const titleLink = document.createElement('a');
          const filter = this.elem.dataset.tagFilter ? (
            this.elem.dataset.tagFilter
              .split(',')
              .map((item) => `#${item.trim()}`)
          ) : [''];
          const className = 'instagram-widget';
          this.elem.classList.add(className);

          list.classList.add(`${className}__list`);
          title.classList.add(`${className}__title`);
          titleLink.setAttribute('href', `https://www.instagram.com/${this.userId}/`);
          titleLink.setAttribute('target', '_blank');
          titleLink.innerText = `${this.title}`;
          title.appendChild(titleLink);
          this.elem.appendChild(title);

          fetch(this.url.getMedia)
            .then((response) => response.json())
            // eslint-disable-next-line
            .then((response) => response.data.filter((item) => filter.map((filterItem) => item.caption.includes(filterItem)).filter((i) => i === true).length > 0))
            .then((result) => {
              const data = result.slice(0, 8);
              data.forEach((item) => {
                const inwidgetItem = document.createElement('div');
                const inwidgetItemContent = document.createElement('div');
                const inwidgetItemLink = document.createElement('a');
                const inwidgetItemImage = document.createElement('img');
                inwidgetItem.className = `${className}__item`;
                inwidgetItemContent.className = `${className}__item-content`;
                inwidgetItemLink.href = item.media_type === 'VIDEO' ? item.thumbnail_url : item.media_url;
                inwidgetItemLink.dataset.fancybox = 'inwidget';
                inwidgetItemImage.src = item.media_type === 'VIDEO' ? item.thumbnail_url : item.media_url;
                inwidgetItemImage.alt = item.caption;
                inwidgetItemLink.appendChild(inwidgetItemImage);
                inwidgetItemContent.appendChild(inwidgetItemLink);
                inwidgetItem.appendChild(inwidgetItemContent);
                list.appendChild(inwidgetItem);
              });
              this.elem.appendChild(list);
            })
            .catch(() => { inwidget.forEach((widget) => widget.remove()); });

          fetch(this.url.refreshToken)
            .then((response) => response.text());
        });
    }
  }

  inwidget.forEach((item) => {
    const widget = new InstaWidget({
      elem: item,
      selector: settings.element,
      userId: settings.userId,
      accessToken: settings.accessToken,
      url: settings.url,
    });
    widget.init();
  });
};

export default instaWidget;
