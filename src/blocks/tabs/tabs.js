export default function tabs() {
  const tabsList = Array.from(document.querySelectorAll('.tabs'));
  tabsList.forEach((block) => {
    const tl = Array.from(block.querySelectorAll('.tabs__label'));
    const tc = Array.from(block.querySelectorAll('.tabs__item'));
    const currentLoc = window.location.href.substr(window.location.href.indexOf('#') + 1);

    const activate = (content, label) => {
      content.classList.add('tabs__item--active');
      label.classList.add('tabs__label--active');
    };

    const deactivate = (content, label) => {
      content.classList.remove('tabs__item--active');
      label.classList.remove('tabs__label--active');
    };

    let isActive = false;

    tl.forEach((label, i) => {
      const tab = tc[i];

      if (label.id === currentLoc) {
        activate(tab, label);
        isActive = true;
      }

      const showHide = () => {
        if (!tc[i].classList.contains('tabs__item--active')) {
          tl.forEach((item, j) => {
            deactivate(tc[j], tl[j]);
          });
          activate(tab, label);
        }
      };

      if (label.id) {
        const links = Array.from(document.querySelectorAll(`a[href='#${label.id}']`));
        links.forEach((link) => {
          link.addEventListener('click', showHide);
        });
      }
      label.addEventListener('click', showHide);
    });

    if (!isActive) {
      activate(tc[0], tl[0]);
    }
  });
}
