export default class Tip {
  constructor(element, linkSelector, bodySelector, activeClass) {
    this.element = element;
    this.linkSelector = linkSelector;
    this.bodySelector = bodySelector;
    this.link = element.querySelector(linkSelector);
    this.body = element.querySelector(bodySelector);
    this.activeClass = activeClass;
  }

  handler() {
    const { element, body } = this;
    const left = element.offsetLeft - element.parentElement.offsetLeft;
    body.setAttribute('style', `--tip-body-left: ${-left}px; --tip-arrow-left: ${left}px`);
  }

  init() {
    const {
      element, body, link, activeClass,
    } = this;
    link.addEventListener('click', (e) => {
      e.preventDefault();
      body.classList.toggle(activeClass);
      const windowWidth = document.documentElement.clientWidth;
      if (element.getBoundingClientRect().left + body.offsetWidth > windowWidth) {
        body.classList.add(`${this.bodySelector.substr(1)}--left`);
      }
    });

    this.handler();

    document.addEventListener('click', (e) => {
      if (e.target !== link && e.target !== body && body.classList.contains(activeClass)) {
        body.classList.remove(this.activeClass);
      }
    });
  }
}
