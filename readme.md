# Шаблон для быстрого старта проекта
## Старт проекта
1. Склонировать репозиторий и перейти в папку проекта
```
git clone https://gitlab.com/medreclama/template имя-проекта && cd имя-проекта
```

3. Установить зависимости
```
npm i
```

4. Запустить шаблон
```
npm start
```

### Сервер будет доступен по адресам:

**localhost:3000** — для доступа с машины, на которой запущен сервер.  
**ip-адрес-в-локальной-сети:3000** (например, 192.168.1.65:3000) — для доступа с других устройств в локалке, например, с телефона.

## Автоматическая сборка на сервере medreclama.gitlab.io
На гитлабе ветка master собирается по правилам production-сборки и заливается на сервер __medreclama.gitlab.io__. После этого собранная вёрстка будет доступна по адресу `medreclama.gitlab.io/имя-проекта`

## Команды
### Запуск с отслеживанием изменений
```
npm start
```
### Сборка в папку `dist`
```
npm run build
```

### Production-cборка в папку `dist`
```
npm run production
```
### Очистка папки `dist`
```
npm run clean
```

## Структура
```
├── src/                       # Исходники
│   ├── blocks/                # Блоки
│   │   └── block/             # Блок
│   │       ├── block.js       # Скрипт блока
│   │       ├── block.pug      # Разметка блока
│   │       └── block.styl     # Стили блока
│   ├── pages/                 # Страницы
│   │   └── index.pug          # Разметка страницы
│   ├── resources/             # Статические файлы для копирования в dist
│   ├── scripts/               # Скрипты
│   │   └── script.js          # Главный скрипт
│   └── styles/                # Стили
│       ├── includes/          # Помощники
│       │   ├── fonts.styl     # Подключение шрифтов
│       │   ├── mixins.styl    # stylus-иксины
│       │   ├── reset.styl     # Сброс стилей
│       │   └── variables.styl # Переменные
│       ├── svg/               # svg файлы для включения в стили
│       └── styles.styl        # Главный файл стилей
├── dist/                      # Сборка (автогенерация)
├── tasks/                     # Задачи для gulpfile.babel.js
│   ├── copy.js                # Копирование
│   ├── default.js             # Итоговые списки задач по умолчанию
│   ├── script.js              # Сборка скриптов 
│   ├── server.js              # Запуск сервера
│   ├── stylus.js              # Сборка стилей в stylus
│   ├── template.js            # Сборка страниц из pug-шаблонов
│   └── watcher.js             # Отслеживание изменений и запуск задач
├── .babelrc                   # Конфигурация Babel
├── .eslintrc                  # Конфигурация ESLint
├── .gitignore                 # Список исключённых из git файлов
├── .huskyrc                   # Конфигурация husky
├── .lintstagedrc              # Конфигурация lint staged
├── .stylintrc                 # Конфигурация stylint
├── gulpfile.babel.js          # Файл для запуска gulp
├── package.json               # Список модулей и прочей информации
├── readme.md                  # Документация шаблона
└── webpack.conf.js            # Конфигурация webpack
```

## Проверка кода
Используются линтеры стилей и скриптов. Для проверки скриптов используется [пресет от airbnb](https://www.npmjs.com/package/eslint-config-airbnb), для проверки стилей собран свой пресет на основе htmlacademy.ru и csssr.ru.

## git-hooks
Используется [husky](https://www.npmjs.com/package/husky). Перед каждым `git commit` запускается линтер. Если линтер падает с ошибкой, `git commit` не пройдёт.

## Импорт svg в css
Сборщик может импортировать svg-файлы напрямую в css.

svg-файлы должны лежать в папке `/src/styles/svg`. __В именах файлов не должно быть пробелов и знаков препинания.__

Для подключения svg нужно в styl-файле вызвать функцию `svgImport('svg/имяФайла.svg')`:
```
.block
  background-image svgImport('svg/fileName.svg')
```
## stylus-миксины
В файле `src/styles/includes/mixins.styl` лежат миксины для упрощения работы.
### Брейкпоинты
Брейкпоинты для использования в медиа-выражениях.
```
point-s = 480px
point-m = 800px
point-l = 1000px
point-xl = 1200px
point-hd = 1600px
```
Указывают на минимальную ширину экрана. Если нужно использовать как максимальную ширину, то надо указывать как `брейкпоинт - 1`:
```
@media screen and (min-width point-s)
  .block
    background red
@media screen and (max-width point-s - 1)
  .block
    background green
```
### Базовый отступ
Используется для указания отступов между элементами по горизонтали.
```
basePadding = .75rem
```
Его использует блок сетки __grid__
### Функции
#### Горизонтальные поля
```
padding-x(n)
```
#### Вертикальные поля
```
padding-y(n)
```
#### Горизонтальные отступы
```
margin-x(n)
```
#### Вертикальные отступы
```
margin-y(n)
```
#### Пример
Конструкция 
```
.block
  padding-x(16px)
  padding-y(16px)
  margin-x(16px)
  margin-y(16px)
```
создаст
```
.block {
  padding-right: 16px;
  padding-left: 16px;
  padding-top: 18px;
  padding-bottom: 18px;
  margin-right: 20px;
  margin-left: 20px;
  margin-top: 22px;
  margin-bottom: 22px;
}
```
## Блоки
Базовые блоки в папке `src/blocks/`.
### classes
Вспомогательная js-функция для формирования классов-модификаторов. Принимает два параметра — строку `класс` и объект с настройками:
```
{
  'название-свойства-1': 'значение',
  'название-свойства-2': true,
}
```
и формирует строку
```
класс класс--название-свойства-1-значение класс--название-свойства-2
```

Если значение свойства — текст или число, формируется модификатор `класс--название-свойства-1-значение`, если значение — `true`, то формируется модификатор `класс--название-свойства-2`.

#### Пример

Конструкция
```
-
  const modifiers = {
    'justify': 'center',
    'align': 'baseline',
    'padding-y': true,
  }

div(class= classes('grid', modifiers))
```
создаст блок
```
<div class="grid grid--justify-center grid--align-baseline grid--padding-y"></div>
```

### create-element
Блок создания элемента из объекта с конфигурацией. Используется в других блоках в случае, если нужно сформировать содержимое из объекта, а не напрямую через pug.
```
+createElement(elem)
```
Новый элемент может быть строковым узлом, блоком или списком узлов.

#### Строка
Для формирования строкового узла в функцию передаётся простая строка:
```
+createElement('lorem ipsum')
```
#### Блок
Для формирования блока в функцию передаётся блок с конфигурацией:

```
-
  const block = {
    'tag': 'a', // Тег элемента. Значение по умолчанию — 'div'
    'className': 'class', // Имя класса
    'attr': { // Объект атрибутов блока. Ключ элемента формирует атрибут, значение элемента — значение атрибута
      href: '#',
    },
    'content': 'ссылка', // Содержимое блока. Может быть текстом, новым блоком или списком элементов
  }
```

#### Пример

Для формирования абзаца со ссылкой внутри понадобится такая конструкция:
```
-
  const par = {
    tag: 'p',
    className: 'paragraph',
    content: [
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore ',
      {
        tag: 'a',
        attr: {href: 'https://google.ru'},
        content: 'magna aliqua'
      }
      '. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
    ]
  }
+createElement(par)
```
### form-input
Поле ввода с label'ом.
```
+formInput(settings)
```
Доступные настройки:
```
-
  const settings = {
    'type': 'text', // Тип поля. Доступные значения: все возможные значения для атрибута type элемента input, textarea и select
    'name': '', // Имя поля
    'required': false, // Определяет, является ли поле обязательным
    'placeholder': '', // placeholder поля
    'readonly': false, // Определяет, доступно ли поле для редактирования
    'maxlength': '', // Максимальное число для поля типа number
    'label': '', // Подпись поля. Может быть текстом или объектом с настройками для функции +createElement(). Если этого свойства нет, то лейбл не формируется
    'value': '' // Значение поля, атрибут value
    'options': [''], // Массив вариантов для поля <select>. Элемент массива — текст или объект с настройками для функции +createElement()
  }
```
### grid
Блок сетки. Состоит из двух функций — `grid` для создания контейнера и `cell` для создания ячейки.
#### Функция grid
```
+grid(настройки)
```
Создаёт флекс-контейнер сетки. Принимает объект с настройками:
```
-
  const gridSettings = {
    'tag': 'div', // Тег элемента. Значение по умолчанию — 'div'
    'align': 'stretch', // Выравнивание блоков по вертикали. Значение по умолчанию — 'stretch'.
      // Возможные значения — значения css-свойства align-items: 'center', 'flex-end', 'flex-begin', 'stretch', 'baseline'
    'justify': 'flex-start' // Выравнивание блоков по вертикали. Значение по умолчанию — 'flex-start'.
      // Возможные значения — значения css-свойства justify-content: 'flex-start', 'flex-end', 'center', 'space-between', 'space-around', 'space-evenly'
    'nopadding': false, // Отключает отступы между элементами. Значение по умолчанию — false
    'padding-y': false // Включает вертикальные отступы между элементами. Значение по умолчанию — false
    'cols': 12, // Количество колонок в сетке. По умолчанию — 12. Для значений кроме 12 и 10 нужно отдельно написать стили
  }
```
#### Функция cell
```
+cell(настройки)
```
Создаёт ячейку сетки. Принимает объект с настройками:
```
-
  const cellSettings = {
    'tag': 'div', // Тег элемента. Значение по умолчанию — 'div'
    'hd': '', // Размер элемента при ширине экрана больше брейкпоинта point-hd
    'xl': '', // Размер элемента при ширине экрана больше брейкпоинта point-xl
    'l': '', // Размер элемента при ширине экрана больше брейкпоинта point-l
    'm': '', // Размер элемента при ширине экрана больше брейкпоинта point-m
    's': '', // Размер элемента при ширине экрана больше брейкпоинта point-s
    'xs': '', // Размер элемента. Если размер элемента не должен меняться при изменении размера экрана, можно указывать только это свойство
  }
```
Размер указывает на то, сколько столбцов занимает ячейка. Устанавливается в диапазоне от 1 до указанного в настройках функции `grid` количества столбцов. То есть, по умолчанию — от 1 до 12.

Если размеры не указаны, автоматически указывается `{'s': 12}`
#### Пример
Для создания раскладки для списка `<ul>`, в которой четыре ячейки на десктопах будут располагаться в одну строку, на планшетах — в две, а на телефонах каждый элемент будет занимать по строке, нужно написать такой код:
```
+grid({'tag': 'ul'})
  +cell({'tag':'li', 'm': 3, 's': 6, 'xs': 12}) test
  +cell({'tag':'li', 'm': 3, 's': 6, 'xs': 12}) test2
  +cell({'tag':'li', 'm': 3, 's': 6, 'xs': 12}) test3
  +cell({'tag':'li', 'm': 3, 's': 6, 'xs': 12}) test4
```
что создаст блок
```
<ul class="grid">
  <li class="grid__cell grid__cell--m-3 grid__cell--s-6 grid__cell--xs-12">test
  </li>
  <li class="grid__cell grid__cell--m-3 grid__cell--s-6 grid__cell--xs-12">test2
  </li>
  <li class="grid__cell grid__cell--m-3 grid__cell--s-6 grid__cell--xs-12">test3
  </li>
  <li class="grid__cell grid__cell--m-3 grid__cell--s-6 grid__cell--xs-12">test4
  </li>
</ul>
```
### head
Блок `<head>`. Содержит базовую информацию о странице — кодировку, meta viewport и ссылку на файл стилей.

Есть возможность внесения title, description и keywords для страницы, а также другой информации в блоке `head`.
* Для указания title, description и keywords нужно в блоке `head` страницы задать значения переменных `pageTitle`, `pageDescription` и `pageKeywords`.

* Для добавления другой информации нужно прописать нужные блоки в блок `head`.

```
block head
  -
    var pageTitle = 'Название страницы'
    var pageDescription = 'Описание страницы'
    var pageKeywords = 'ключевые слова, страница'
  link(rel='stylesheet' href='/template/styles/add.css')
```
### iframe-responsive
Блок отзывчивого iframe с произвольным контентом. Принимает два строковых параметра — url и описание. Формирует блок с iframe и описанием.

#### Пример

Конструкция
```
+iframeResponsive('https://www.youtube.com/embed/jNQXAC9IVRw', 'Me at the zoo')
```
создаст блок
```
<div class="iframe-responsive">
  <div class="iframe-responsive__iframe">
    <iframe src="https://www.youtube.com/embed/jNQXAC9IVRw" allowfullscreen></iframe>
  </div>
  <div class="iframe-responsive__desc">
    Me at the zoo
  </div>
</div>
```
#### youtubeResponsive
Для вставки видео с ютуба есть дополнительная функция youtubeResponsive(), которая вместо url принимает id видео:
```
+youtubeResponsive('jNQXAC9IVRw', 'Me at the zoo')
```
Эта функция формирует тот же блок, что и в предыдущем примере.
### layout
Базовая раскладка страницы.
### menu
Формирует двухуровневую структуру меню. Принимает список с элементами.
```
-
  const test = [
    {
      'name':'пункт 1', // Имя пункта
      'href':'page-1.html', // Адрес ссылки
      'active':true, // Активность пункта
      'sublist': [ // Список вложенный пунктов
        {'name':'вложенный пункт 1','href':'/inner-page-1.html'}, // Вложенный пункт
        {'name':'вложенный пункт 2','href':'/inner-page-2.html'},
      ],
    },
    {'name':'пункт 2','href':'/page-2.html'},
    {'name':'пункт 3','href':'/page-3.html'},
    {'name':'пункт 4','href':'/page-4.html'},
  ]
+menu(test)
```
### scripts
Блок подключения скриптов.
```
+scripts(списокСкриптов)
```
Принимает список строк. Каждая строка — имя js-файла в папке `/template/scripts` или ссылка на внешний скрипт.
#### Пример
Конструкция 
```
+scripts(['https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', 'scripts.js', 'add.js'])
```
создаст
```
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="/template/scripts/scripts.js"></script>
<script src="/template/scripts/add.js"></script>
```
### show-hide
js-модуль для показа-скрытия элементов по клику. Для работы нужно добавить атрибут `data-mrc-sh-title="значение"` к элементу, при нажатии на который будет показываться скрытый контент. Скрываемому контенту нужно добавить атрибут `data-mrc-sh-body="значение"`. Значения атрибутов должны совпадать.

У нескольких элементов могут совпадать значения атрибутов `data-mrc-sh-title` и `data-mrc-sh-body`, тогда на странице будет несколько элементов, при нажатии на которые будет открываться один и тот же блок, или несколько скрытых блоков, открывающихся по нажатию на один элемент.

#### Пример

```
a(href='#' data-mrc-sh-title='show-hide') Показать
p(data-mrc-sh-body='show-hide') Скрытый контент
```
### table
Блок таблицы. Для формирования таблицы используются миксины с названиями, повторяющими имена соответствующих тегов. Для миксинов указываются те же атрибуты, что и для тегов.

Таблица оборачивается в блок `.table-wrap` для адаптивной стилизации. Также формируется группа тегов `colgroup` для стилизации столбцов.

#### Пример

Конструкция
```
+table
  +tr
    +th test
    +th test2
  +tr
    +td test3
    +td test23
  +tr
    +td(colspan=2) test3
  +tr
    +td test3
    +td(rowspan=2) test23
  +tr
    +td test3
```
создаст блок
```
<div class="table-wrap">
  <table class="table">
    <colgroup class="table__colgroup table__colgroup--2"> 
      <col class="table__col">
      <col class="table__col">
    </colgroup>
    <tbody><tr class="table__row">
      <th class="table__header">Заголовок 1
      </th>
      <th class="table__header">Заголовок 2
      </th>
    </tr>
    <tr class="table__row">
      <td class="table__cell">ячейка 1
      </td>
      <td class="table__cell">ячейка 2
      </td>
    </tr>
    <tr class="table__row">
      <td class="table__cell" colspan="2">ячейка 3
      </td>
    </tr>
    <tr class="table__row">
      <td class="table__cell">ячейка 4
      </td>
      <td class="table__cell" rowspan="2">ячейка 5
      </td>
    </tr>
    <tr class="table__row">
      <td class="table__cell">ячейка 6
      </td>
    </tr>
  </tbody></table>
</div>
```
### tabs
pug-миксин табов. Сначала вызывается блок `+tabs()`, затем вложенные в него блоки `+tab({name:'название таба'})`.

#### Пример

Конструкция
```
+tabs()
  +tab({'name': 'Первый таб'})
    p содержимое первого таба
  +tab({'name': 'Второй таб'})
    p содержимое второго таба
```
создаст блок
```
<div class="tabs">
  <div class="tabs__labels">
    <div class="tabs__label tabs__label--active">Первый таб</div>
    <div class="tabs__label">
      <p>содержимое первого таба</p>
    </div>
  </div>
  <div class="tabs__contents">
    <div class="tabs__item">Второй таб
    </div>
    <div class="tabs__item">
      <p>содержимое второго таба</p>
    </div>
  </div>
</div>
```
